"""
Arquivo: customerexchange.py
Autor: Michael Dougras da Silva

Descrição:
Arquivo de implementação da estrutura responsável por calcular a vizinhança considerando o movimento de
troca entre pares de passageiros.

Changelog:
    31.05.2016  Michael     Criação do arquivo e primeiras adequações para resolução do problema.
    01.06.2016  Michael     Mudança na avaliação da lista tabu, com nova implementação.
    02.06.2016  Michael     Mudança brusca na implementação, agora a instância pode ser reaproveitada, preciso atualizar
                            a documentação da classe.
"""

# Área de importações
import math
from bisect import bisect


class CustomerExchangeNH:
    """
    Classe de cálculo dos movimentos possíveis considerando a vizinhança imposta pelo movimento
    de troca entre dois passageiros. Sendo assim, motoristas inclusos na atual solução não são
    considerados no movimento de troca. Instâncias desta classe não devem ser reaproveitadas após
    a modificação da solução atual. Você deve criar novas instâncias para atualização dos pesos de
    impacto dos movimento após atualizações na solução avaliada.
    """

    def __init__(self, ltcpp, solution, tabulist):
        """
        Construtor da classe
        :param ltcpp: Instância do problema ltcpp, instância da classe LTCPP.
        :param solution: Solução atual sendo avaliada, instância da classe Solution.
        :param tabulist: Lista tabu com os últimos movimentos efetuados.
        """
        # Captura a lista de elementos presentes no problema (exclui o primeiro elemento que é o destino)
        self._customers = sorted(ltcpp.graph.nodes()[1:])
        self._ltcpp = ltcpp
        self._solution = solution
        self._tabulist = tabulist

        # Dicionário de armazenamento de valores de impacto após os movimentos serem efetuados
        self._movements = {i: {j: math.inf for j in self._customers[self._customers.index(i) + 1:]} for i in self._customers[:-1]}

        self._initialize_movements()

    def _initialize_movements(self):
        """
        Método de cálculo dos impactos de cada combinação de movimento possível para este tipo de vizinhança.
        """

        # Testa todas as combinações de trocas, desconsidera movimentos simétricos(a-b é igual a b-a)
        for i in self._customers[:-1]:
            for j in self._customers[self._customers.index(i) + 1:]:
                self._calculate_movement(i, j)

    def _calculate_movement(self, i, j):
        drivers = self._solution.get_drivers()
        # A troca envolvendo motoristas é inviável nesta vizinhança, e testa se o movimento está na lista tabu
        if (i in drivers) or (j in drivers) or (not self._tabulist.can_exchange(i, j)):
            self._movements[i][j] = math.inf
        # Calculando impacto de troca entre passageiros
        else:
            # Captura os grupos dos envolvidos na troca
            gi = self._solution.get_node_group(i)
            gj = self._solution.get_node_group(j)

            # Tenho que verificar se os dois nós estão no mesmo grupo
            if gi == gj:
                # Considera cálculo somente dentro de um grupo
                aux = gi.components[:]
                indexi = aux.index(i)
                indexj = aux.index(j)
                aux[indexi], aux[indexj] = aux[indexj], aux[indexi]
                diff = self._ltcpp.get_path_distance(aux) - gi.bestw
            else:
                # Considera cálculo entre dois grupos
                before_exchange = gi.bestw + gj.bestw
                auxgi = gi.components[:]
                auxgj = gj.components[:]
                auxgi[auxgi.index(i)] = j
                auxgj[auxgj.index(j)] = i
                after_exchange = self._ltcpp.get_path_distance(auxgi) + self._ltcpp.get_path_distance(auxgj)
                diff = after_exchange - before_exchange

            # Guarda registro do movimento calculado
            self._movements[i][j] = diff

    def get_movements(self):
        """
        Retorna a matriz de movimentos.
        :return:
        Matriz com as combinações de movimentos possíveis.
        """
        return self._movements

    def do_best_movement(self):
        last_solution_bestw = self._solution.bestw
        bestw = math.inf
        bestindexes = ()

        # Procura o melhor movimento
        for i in self._customers[:-1]:
            for j in self._customers[self._customers.index(i) + 1:]:
                if self._movements[i][j] < bestw:
                    bestw = self._movements[i][j]
                    bestindexes = (i, j)

        if bestw == math.inf:
            return None

        # Captura os envolvidos na troca
        i, j = bestindexes
        # Captura os grupos dos envolvidos na troca
        gi = self._solution.get_node_group(i)
        gj = self._solution.get_node_group(j)

        # Tenho que verificar se os dois nós estão no mesmo grupo
        if gi == gj:
            # Considera troca dentro de um grupo
            aux = gi.components[:]
            indexi = aux.index(i)
            indexj = aux.index(j)
            aux[indexi], aux[indexj] = aux[indexj], aux[indexi]
            bestw = self._ltcpp.get_path_distance(aux)
            self._solution.remove_group(gi)
            self._solution.add_group(aux, bestw)
        else:
            # Considera troca entre dois grupos
            auxgi = gi.components[:]
            auxgj = gj.components[:]
            auxgi[auxgi.index(i)] = j
            auxgj[auxgj.index(j)] = i
            bestwi, bestwj = self._ltcpp.get_path_distance(auxgi), self._ltcpp.get_path_distance(auxgj)

            # Remove grupos antigos
            self._solution.remove_group(gi)
            self._solution.remove_group(gj)
            self._solution.add_group(auxgi, bestwi)
            self._solution.add_group(auxgj, bestwj)

        self._tabulist.register_exchange(i, j)
        # assert math.isclose((self._solution.bestw - last_solution_bestw),
        #                     self._movements[i][j]), 'Compute error, CustomerExchange {} <> {}'.format(
        #     (self._solution.bestw - last_solution_bestw), self._movements[i][j])
        return i,j


    def recalculate_movement(self, node):
        customers_before = self._customers[:bisect(self._customers, node) - 1]
        customers_after = self._customers[bisect(self._customers, node):]

        j = node
        for i in customers_before:
            self._calculate_movement(i, j)

        i = node
        for j in customers_after:
            self._calculate_movement(i, j)