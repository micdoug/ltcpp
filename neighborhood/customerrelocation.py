"""
Arquivo: customerrelocation.py
Autor: Michael Dougras da Silva

Descrição:
Arquivo de implementação da estrutura responsável por calcular a vizinhança considerando o movimento de
troca de posicionamento de um passageiro.

Changelog:
    31.05.2016  Michael     Criação do arquivo e primeiras adequações para resolução do problema.
                            Adicionado penalidade no caso de um grupo ficar com somente um elemento após a realocação.
    01.06.2016  Michael     Mudança na avaliação da lista tabu, com nova implementação.
    02.06.2016  Michael     Mudança brusca na implementação, agora a instância pode ser reaproveitada, preciso atualizar
                            a documentação da classe.
"""

# Área de importação
from .movement import Movement
from core import config
import math


class CustomerRelocationNH:
    """
    Classe de cálculo dos movimentos possíveis considerando a vizinhança imposta pelo movimento
    de troca de posicionamento de um passageiro. Sendo assim, motoristas inclusos na atual solução não são
    considerados no movimento. Instâncias desta classe não devem ser reaproveitadas após
    a modificação da solução atual. Você deve criar novas instâncias para atualização dos pesos de
    impacto dos movimento após atualizações na solução avaliada.
    """
    def __init__(self, ltcpp, solution, tabulist):
        """
        Construtor da classe
        :param ltcpp: Instância do problema ltcpp, instância da classe LTCPP.
        :param solution: Solução atual sendo avaliada, instância da classe Solution.
        :param tabulist: Referência para a lista tabu atual.
        """
        # Captura a lista de elementos presentes no problema (exclui o primeiro elemento que é o destino)
        self._customers = ltcpp.graph.nodes()[1:]
        self._ltcpp = ltcpp
        # Lista para armazenar os movimentos calculados
        self._movements = {i: {j: math.inf for j in self._customers} for i in self._customers}
        self._solution = solution
        self._tabulist = tabulist
        self._initialize_movements()

    def _initialize_movements(self):
        """
        Método de cálculo dos impactos de cada combinação de movimento possível para este tipo de vizinhança.
        """

        # Testa todas as combinações de posicionamento possível, considerando a possibilidade de posicionar o passageiro
        # i antes de j.
        for i in self._customers:
            for j in self._customers:
                self._calculate_movement(i, j)

    def _calculate_movement(self, i, j):
        drivers = self._solution.get_drivers()
        # O passageiro ser realocado consigo mesmo não faz sentido, e se ele for motorista não pode ser trocado
        # E se estiver na lista tabu também não pode ser realocado
        if (i == j) or (i in drivers) or (j in drivers) or (not self._tabulist.can_relocate(i)):
            self._movements[i][j] = math.inf
        # Calculando impacto de reposicionamento
        else:
            # Captura os grupos dos envolvidos na troca
            gi = self._solution.get_node_group(i)
            gj = self._solution.get_node_group(j)

            diff = math.inf

            # Tenho que verificar se os dois nós estão no mesmo grupo
            if gi == gj:
                indexj = gi.components.index(j)
                indexi = gi.components.index(i)
                if (indexi+1) == indexj:
                    diff = math.inf
                else:
                    # Considera cálculo somente dentro de um grupo
                    aux = gi.components[:]
                    aux.remove(i)
                    indexj = aux.index(j)
                    aux.insert(indexj, i)
                    diff = self._ltcpp.get_path_distance(aux) - gi.bestw

            # Se o grupo de j já está completo, não há como fazer movimentos deste tipo
            elif len(gj.components) < config.vehicle_capacity:
                # Considera cálculo entre dois grupos
                before_exchange = gi.bestw + gj.bestw
                auxgi = gi.components[:]
                auxgj = gj.components[:]
                indexj = auxgj.index(j)

                # Remove i do seu grupo de origem
                auxgi.remove(i)
                # Insere i antes de j
                auxgj.insert(indexj, i)

                # Adicionado noção de penalidade quando um grupo fica com somente um elemento
                after_exchange = self._ltcpp.get_path_distance(auxgi) * (1.5 if len(auxgi) == 1 else 1) + \
                                 self._ltcpp.get_path_distance(auxgj)
                diff = after_exchange - before_exchange

            # Armazena o movimento calculado
            self._movements[i][j] = diff

    def get_movements(self):
        """
        Retorna uma lista com os possíveis movimentos, ordenados de maneira crescente
        em relação ao peso de impacto. Se todos os grupos estiverem com lotação total,
        não será possível efetuar este movimento.
        :return:
        Lista com movimentos possíveis
        """
        return self._movements

    def do_best_movement(self):
        last_solution_bestw = self._solution.bestw
        bestw = math.inf
        bestindexes = ()

        # Procura o melhor movimento
        for i in self._customers:
            for j in self._customers:
                if self._movements[i][j] < bestw:
                    bestw = self._movements[i][j]
                    bestindexes = (i, j)

        if bestw == math.inf:
            return None
        if not bestindexes:
            return []

        # Captura os envolvidos na realocação
        i, j = bestindexes

        # Captura os grupos dos envolvidos na troca
        gi = self._solution.get_node_group(i)
        gj = self._solution.get_node_group(j)

        # Tenho que verificar se os dois nós estão no mesmo grupo
        if gi == gj:
            # Efetua a realocação dentro de um grupo
            aux = gi.components[:]
            aux.remove(i)
            indexj = aux.index(j)
            aux.insert(indexj, i)
            self._solution.remove_group(gi)
            self._solution.add_group(aux, self._ltcpp.get_path_distance(aux))
            nodes = (i,)

        else:
            # Efetua realocação entre dois grupos
            auxgi = gi.components[:]
            auxgj = gj.components[:]
            indexj = auxgj.index(j)

            # Remove i do seu grupo de origem
            auxgi.remove(i)
            # Insere i antes de j
            auxgj.insert(indexj, i)

            self._solution.remove_group(gi)
            self._solution.remove_group(gj)

            self._solution.add_group(auxgi, self._ltcpp.get_path_distance(auxgi) * (1.5 if len(auxgi) == 1 else 1)) # multiplica o fator de penalidade
            self._solution.add_group(auxgj, self._ltcpp.get_path_distance(auxgj))
            nodes = (i, auxgi[0])   # Para ter uma referencia de alguém do antigo grupo de i para poder atualizar de fora

        self._tabulist.register_relocate(i)
        # assert math.isclose((self._solution.bestw - last_solution_bestw),
        #                     self._movements[i][j]), 'Compute error, CustomerRelocation {} <> {} \n {} {}'.format(
        #     (self._solution.bestw - last_solution_bestw), self._movements[i][j], aux, (i, j))
        return nodes


    def recalculate_movement(self, node):
        for i in self._customers:
            self._calculate_movement(i, node)

        for j in self._customers:
            self._calculate_movement(node, j)
