"""
Arquivo: routeexchange.py
Autor: Michael Dougras da Silva

Descrição:
Arquivo de implementação da estrutura responsável por calcular a vizinhança considerando o movimento de
troca de subcaminhos entre diferentes rotas ou grupos.

Changelog:
    31.05.2016  Michael     Criação do arquivo e primeiras adequações para resolução do problema.
    01.06.2016  Michael     Mudança na avaliação da lista tabu, com nova implementação.
"""

# Área de importação
from .movement import Movement
from core import config
from collections import OrderedDict
import math


class RouteExchangeNH:
    """
    Classe de cálculo dos movimentos possíveis considerando a vizinhança imposta pelo movimento
    de troca de subcaminhos entre rotas diferentes. Instâncias desta classe não devem ser reaproveitadas após
    a modificação da solução atual. Você deve criar novas instâncias para atualização dos pesos de
    impacto dos movimento após atualizações na solução avaliada.
    """
    def __init__(self, ltcpp, solution, tabulist):
        """
        Construtor da classe
        :param ltcpp: Instância do problema ltcpp, instância da classe LTCPP.
        :param solution: Solução atual sendo avaliada, instância da classe Solution.
        """
        # Captura a lista de elementos presentes no problema (exclui o primeiro elemento que é o destino)
        self._customers = ltcpp.graph.nodes()[1:]
        self._ltcpp = ltcpp
        self._solution = solution
        # Armazena os ids de todos os grupos avaliados nesta vizinhança
        self._groups = OrderedDict({g.id: g for g in self._solution.get_groups()})
        keys = list(self._groups.keys())
        # Lista para armazenar movimentos calculados
        self._movements = {i: {j: [] for j in keys[keys.index(i)+1:]} for i in keys[:-1]}
        self._tabulist = tabulist
        self._initialize_movements()

    def _initialize_movements(self):
        """
        Método de cálculo dos impactos de cada combinação de movimento possível para este tipo de vizinhança.
        """
        keys = list(self._groups.keys())

        # Every single pair combination of groups excluding symetric exchanges
        for g1k in keys[:-1]:
            for g2k in keys[keys.index(g1k)+1:]:
                self._calculate_movement(g1k, g2k)

    def _calculate_movement(self, g1k, g2k):
        g1 = self._groups[g1k]
        g2 = self._groups[g2k]
        # Percorre todas as combinações de subcaminhos entre os dois grupos atuais
        for i in range(1, len(g1.components)):
            for j in range(1, len(g2.components)):

                # Verifica lista tabu se algum elemento está bloqueado
                tabu = False
                for n in g1.components[i:]:
                    if not self._tabulist.can_relocate(n):
                        tabu = True
                        break
                for m in g2.components[j:]:
                    if not self._tabulist.can_relocate(m):
                        tabu = True
                        break

                diff = math.inf
                # Tenta efetuar troca entre subcaminhos
                auxg1 = g1.components[:i] + g2.components[j:]
                auxg2 = g2.components[:j] + g1.components[i:]
                if (not tabu) and (len(auxg1) <= config.vehicle_capacity) and (len(auxg2) <= config.vehicle_capacity):
                    before = g1.bestw + g2.bestw
                    after = self._ltcpp.get_path_distance(auxg1) + self._ltcpp.get_path_distance(auxg2)
                    diff = after - before
                # Armazena movimento gerado
                self._movements[g1k][g2k].append(Movement([g1.components[i:], g2.components[j:]], diff))

    def recalculate_movement(self):
        # Try to find the ids of the removed groups
        old_ids = {k for k in self._groups.keys()}
        new_ids = {g.id for g in self._solution.get_groups()}

        remove_ids = old_ids - new_ids
        add_ids = new_ids - old_ids

        # Remove groupos inválidos da matriz de movimentos
        for id in remove_ids:
            del self._groups[id]
            if id in self._movements:
                del self._movements[id]
            for i in old_ids:
                if i in self._movements and id in self._movements[i]:
                    del self._movements[i][id]

        new_groups = [g for g in self._solution.get_groups() if g.id in add_ids]
        for new in new_groups:
            self._groups[new.id] = new

        # Adiciona combinações com grupos novos
        for id in self._groups.keys():
            for new in add_ids:
                if id == new:
                    continue
                if id not in self._movements:
                    self._movements[id] = {}
                self._movements[id][new] = []
                self._calculate_movement(id, new)



    def get_movements(self):
        """
        Retorna uma lista com os possíveis movimentos, ordenados de maneira crescente
        em relação ao peso de impacto.
        :return:
        Lista com movimentos possíveis
        """
        return self._movements

    def do_best_movement(self):

        last_solution_bestw = self._solution.bestw
        keys = list(self._groups.keys())
        best_movement = Movement([], math.inf)

        # Every single pair combination of groups excluding symetric exchanges
        for g1k in keys[:-1]:
            for g2k in keys[keys.index(g1k) + 1:]:
                for movement in self._movements[g1k][g2k]:
                    if movement.impact < best_movement.impact:
                        best_movement = movement

        if best_movement.impact == math.inf:
            return None

        subi, subj = best_movement.nodes

        gi = self._solution.get_node_group(subi[0])
        gj = self._solution.get_node_group(subj[0])

        auxi = gi.components[:gi.components.index(subi[0])] + subj
        auxj = gj.components[:gj.components.index(subj[0])] + subi

        self._solution.remove_group(gi)
        self._solution.remove_group(gj)
        self._solution.add_group(auxi, self._ltcpp.get_path_distance(auxi))
        self._solution.add_group(auxj, self._ltcpp.get_path_distance(auxj))

        for i in subi:
            self._tabulist.register_relocate(i)
        for j in subj:
            self._tabulist.register_relocate(j)

        # assert math.isclose((self._solution.bestw - last_solution_bestw),
        #                     best_movement.impact), 'Compute error, RouteExchange {} <> {}'.format(
        #     (self._solution.bestw - last_solution_bestw), best_movement)

        return tuple(auxi + auxj)
