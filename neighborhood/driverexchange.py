"""
Arquivo: driverexchange.py
Autor: Michael Dougras da Silva

Descrição:
Arquivo de implementação da estrutura responsável por calcular a vizinhança considerando o movimento de
troca entre motoristas.

Changelog:
    31.05.2016  Michael     Criação do arquivo e primeiras adequações para resolução do problema.
    01.06.2016  Michael     Mudança na avaliação da lista tabu, com nova implementação.
    02.06.2016  Michael     Mudança brusca na implementação, agora a instância pode ser reaproveitada, preciso atualizar
                            a documentação da classe.
"""

# Área de importações
import math
from bisect import bisect

class DriverExchangeNH:
    """
    Classe de cálculo dos movimentos possíveis considerando a vizinhança imposta pelo movimento
    de troca entre dois motoristas. Instâncias desta classe não devem ser reaproveitadas após
    a modificação da solução atual. Você deve criar novas instâncias para atualização dos pesos de
    impacto dos movimento após atualizações na solução avaliada.
    """

    def __init__(self, ltcpp, solution, tabulist):
        """
        Construtor da classe
        :param ltcpp: Instância do problema ltcpp, instância da classe LTCPP.
        :param solution: Solução atual sendo avaliada, instância da classe Solution.
        :param tabulist: Referência para a lista tabu sendo avaliada.
        """
        self._ltcpp = ltcpp
        self._drivers= sorted(self._ltcpp.drivers)
        # Lista de armazenamento dos movimentos possíveis
        self._movements = {i: {j: math.inf for j in self._drivers[self._drivers.index(i)+1:]} for i in self._drivers[:-1]}
        self._solution = solution
        self._tabulist = tabulist
        self._initialize_movements()

    def _initialize_movements(self):
        """
        Método de cálculo dos impactos de cada combinação de movimento possível para este tipo de vizinhança.
        """
        # Testa todas as combinações de trocas, desconsiderando soluções simétricas (a-b é o mesmo que b-a, neste caso)
        for i in self._drivers[:-1]:
            for j in self._drivers[self._drivers.index(i) + 1:]:
                self._calculate_movement(i, j)

    def _calculate_movement(self, i, j):
        # Verifica se o movimento está bloqueado na lista tabu
        if (i not in self._solution.get_drivers() and j not in self._solution.get_drivers()) or \
                not self._tabulist.can_relocate(i) or not self._tabulist.can_relocate(j):
            self._movements[i][j] = math.inf
        else:
            # Captura os grupos dos envolvidos na troca
            gi = self._solution.get_node_group(i)
            gj = self._solution.get_node_group(j)

            # Tenho que verificar se os dois nós estão no mesmo grupo
            if gi == gj:
                # Considera cálculo somente dentro de um grupo
                aux = gi.components[:]
                indexi = aux.index(i)
                indexj = aux.index(j)
                aux[indexi], aux[indexj] = aux[indexj], aux[indexi]
                diff = self._ltcpp.get_path_distance(aux) - gi.bestw
            else:
                # Considera cálculo entre dois grupos
                before_exchange = gi.bestw + gj.bestw
                auxgi = gi.components[:]
                auxgj = gj.components[:]
                auxgi[auxgi.index(i)] = j
                auxgj[auxgj.index(j)] = i
                after_exchange = self._ltcpp.get_path_distance(auxgi) + self._ltcpp.get_path_distance(auxgj)
                diff = after_exchange - before_exchange

            # Guarda registro do movimento calculado
            self._movements[i][j] = diff

    def get_movements(self):
        """
        Retorna uma lista com os possíveis movimentos, ordenados de maneira crescente
        em relação ao peso de impacto.
        :return:
        Lista com movimentos possíveis
        """
        return self._movements

    def do_best_movement(self):
        last_solution_bestw = self._solution.bestw
        bestw = math.inf
        bestindexes = ()

        # Testa todas as combinações de trocas, desconsiderando soluções simétricas (a-b é o mesmo que b-a, neste caso)
        for i in self._drivers[:-1]:
            for j in self._drivers[self._drivers.index(i) + 1:]:
                if self._movements[i][j] < bestw:
                    bestw = self._movements[i][j]
                    bestindexes = (i, j)

        if bestw == math.inf:
            return None
        if not bestindexes:
            return []

        # Captura os envolvidos na realocação
        i, j = bestindexes

        # Captura os grupos dos envolvidos na troca
        gi = self._solution.get_node_group(i)
        gj = self._solution.get_node_group(j)

        # Tenho que verificar se os dois nós estão no mesmo grupo
        if gi == gj:
            # Considera troca dentro de um grupo
            aux = gi.components[:]
            indexi = aux.index(i)
            indexj = aux.index(j)
            aux[indexi], aux[indexj] = aux[indexj], aux[indexi]
            bestw = self._ltcpp.get_path_distance(aux)
            self._solution.remove_group(gi)
            self._solution.add_group(aux, bestw)
        else:
            # Considera troca entre dois grupos
            auxgi = gi.components[:]
            auxgj = gj.components[:]
            auxgi[auxgi.index(i)] = j
            auxgj[auxgj.index(j)] = i
            bestwi, bestwj = self._ltcpp.get_path_distance(auxgi), self._ltcpp.get_path_distance(auxgj)

            # Remove grupos antigos
            self._solution.remove_group(gi)
            self._solution.remove_group(gj)
            self._solution.add_group(auxgi, bestwi)
            self._solution.add_group(auxgj, bestwj)

        self._tabulist.register_exchange(i, j)
        # assert math.isclose((self._solution.bestw - last_solution_bestw),
        #                     self._movements[i][j], rel_tol=0.01), 'Compute error, CustomerExchange {} <> {}\n {} \n{}'.format(
        #     (self._solution.bestw - last_solution_bestw), self._movements[i][j], (i, j), (gi, gj))
        return i,j

    def recalculate_movement(self, node):
        # Não deve atualizar um nó que não é candidato a motorista
        if node not in self._drivers:
            return

        drivers_before = self._drivers[:bisect(self._drivers, node) - 1]
        drivers_after = self._drivers[bisect(self._drivers, node):]

        j = node
        try:
            for i in drivers_before:
                self._calculate_movement(i, j)
        except Exception as e:
            print('{} |{}|'.format(i, j))

        i = node

        try:
            for j in drivers_after:
                self._calculate_movement(i, j)

        except Exception as e:
            print('|{}| {}'.format(i, j))
