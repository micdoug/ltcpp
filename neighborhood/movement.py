"""
Arquivo: movement.py
Autor: Michael Dougras da Silva

Descrição:
Definição de uma classe simples somente leitura que guarda os dados referentes a um movimento.
Ela é usada pelas implementações de vizinhança.

Changelog:
    31.05.2016  Michael     Criação do arquivo e primeiras adequações para resolução do problema.
"""

from collections import namedtuple

Movement = namedtuple('Impact', ['nodes', 'impact'])