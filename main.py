"""
Arquivo: main.py
Autor: Michael Dougras da Silva

Descrição:
Arquivo de inicialização do programa. Aqui são definidas as instruções o ponto inicial de execução 
do programa.

Changelog:
    31.05.2016  Michael     Criação do arquivo e primeiras adequações para resolução do problema.
    24.06.2016  Michael     Revisão e documentação.
"""

import neighborhood as nb
from core import Arguments, TabuSearch, LTCPP, SimulatedAnnealing, config
import math
from timeit import default_timer

#Criando código para criar o grafo representativo do problema do carpooling

if __name__ == '__main__':
    # Recebe os parâmetros da aplicação
    try:
        arg = Arguments()
    except AttributeError as e:
        print('You have to provide the input and output file names as parameters')
        exit(1)

    # Cria um grafo com a descrição da instância
    ltcpp = LTCPP(arg['finput'], arg['foutput'])
    ts = TabuSearch(ltcpp)
    sa = SimulatedAnnealing(ltcpp)
    ts_sol, ts_time = ts.solve()
    sa_sol, sa_time = sa.solve()
    with open(ltcpp.output, 'w', encoding='utf8') as file:
        file.write('Tabu Search: ' + str(ts_sol.bestw) + '\n' + str(ts_sol.get_groups()) + '\n')
        file.write('Simulated Annealing: ' + str(sa_sol.bestw) + '\n' + str(sa_sol.get_groups()) + '\n')
