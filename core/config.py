"""
Arquivo: config.py
Autor: Michael Dougras da Silva

Descrição:
Arquivo com as definições de parâmetros de configuração da busca tabu.

Changelog:
    31.05.2016  Michael     Criação do arquivo e primeiras adequações para resolução do problema.
"""

# Configurações específicas da Busca Tabu
vehicle_capacity = 5      # Capacidade dos veículos
tabu_size = 10            # Tamanho da lista tabu
max_iter = 2500           # Número máximo de iterações da busca, a classe ltcpp vai ajustar
                          # este valor para 5000 caso a quantidade de participantes seja maior
                          # que 50
unmax_iter = 20           # Número máximo de iterações sem melhorar o resultado, o algoritmo
                          # se encerra quando este valor é alcançado
tabu_target = 1        # Especifica qual tipo de lista tabu será utilizada. Possíveis valores
                          # 1 -> Condena movimentos de troca de participantes
                          # 2 -> Condena movimentos de realocação de passageiros

# Configurações específicas do Simulated Annealing
initial_temp = 100         # Temperatura inicial
decrease_temp = 0.95        # Taxa em percentagem de atualização da temperatura