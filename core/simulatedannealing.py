"""
Arquivo: simulatedannealing.py
Autor: Michael Dougras da Silva

Descrição:
Implementação dos métodos principais da busca tabu.

Changelog:
    24.06.2016  Michael     Criação do arquivo e primeiras adequações para resolução do problema.
"""

import random
from core import LTCPP
from core import Solution
from core import config
import math
from copy import deepcopy
from timeit import default_timer
from concurrent.futures import ProcessPoolExecutor

class SimulatedAnnealing:

    def __init__(self, instance):
        """
        Construtor da classe. 
        :param instance: Uma referência para a instância do problema sendo investigada.
        :type instance: LTCPP
        """
        self.instance = instance

    def _build_initial_solutions(self):
        """
        Constrói soluções iniciais, onde será executada a busca tabu.
        :return:
        Lista com soluções iniciais.
        """
        solutions = []
        for k in range(self.instance.mindrivers, self.instance.maxdrivers+1):
            s = Solution()
            groups = self._dp(k)
            for g in groups:
                temp = self._minimum_path(g, groups[g])
                s.add_group(*temp)
            solutions.append(s)
        return solutions

    def _dp(self, ndrivers):
        """
        Deve ser chamado para construir a solução inicial para um determinado número de motoristas.
        :param ndrivers:
        Número de motoristas a ser testado.
        :return:
        Dicionário com motoristas e passageiros vinculados a ele.
        """
        return self._driver_selection_passenger_assignment(ndrivers, self.instance.graph.nodes()[1:], self.instance.drivers)

    def _driver_selection_passenger_assignment(self, ndrivers, p, d):
        """
        Método de seleção de motoristas e alocação de passageiros.
        :param ndrivers:
        Número de motoristas a ser avaliado.
        :param p:
        Lista de participantes (motoristas + passageiros).
        :param d:
        Lista de motoristas disponíveis.
        :return:
        Lista de motoristas e passageiros atribuídos a eles.
        """
        driver_list = {s: [] for s in random.sample(d, ndrivers)}
        passenger_list = p[:]
        for driver in driver_list:
            passenger_list.remove(driver)

        for passenger in passenger_list:
            min = math.inf
            id = 0
            for driver in driver_list:
                dist = self.instance.graph.edge[passenger][driver]['d']
                if min > dist and (config.vehicle_capacity-1) > len(driver_list[driver]):
                    min = dist
                    id = driver
            driver_list[id].append(passenger)

        return driver_list
        
    def _build_initial_solution(self, ndrivers):
        # Captura um grupo randômico de motoristas dos disponíveis
        drivers = random.sample(self.instance.drivers, ndrivers)
        solution = Solution()
        passengers = [p for p in self.instance.graph.nodes()[1:] if p not in drivers]
        for driver in drivers:
            members = [driver]
            while len(members) < config.vehicle_capacity and passengers:
                members.append(passengers.pop(0))
            
            temp = self._minimum_path(members[0], members[1:])
            solution.add_group(*temp)
        return solution

    def _minimum_path(self, driver, passengers):
        """
        Constrói o melhor caminho utilizando como ponto de origem o motorista de um grupo,
        passando por todos os passageiros e chegando no destino.
        :param driver:
        Motorista.
        :param passengers:
        Lista de passageiros.
        :return:
        """
        # Armazenar melhor solução explorada até agora
        solution = []
        bestw = math.inf

        # Armazenar estado atual da busca na árvore de soluções
        path_sum = 0
        path_order = [driver]

        hops = len(passengers) + 1

        bestw = self._minimum_path_bt(passengers, path_sum, path_order, solution, bestw, hops)
        return solution, bestw

    def _minimum_path_bt(self, passengers, path_sum, path_order, solution, bestw, hops):
        """
        Método de tentativa e erro para encontrar o menor caminho a partir de um motorista, passando
        por todos os passageiros e indo até o destino.
        :param passengers:
        Lista de passageiros a serem analisados.
        :param path_sum:
        Soma do caminho atual explorado.
        :param path_order:
        Ordem de visita do caminho explorado atualmente.
        :param solution:
        Armazena a melhor solução atual.
        :param bestw:
        Armazena o espaço gasto pela melhor solução encontrada até agora.
        :param hops:
        Armazena a quantidade máxima de saltos presentes do caminho a ser construído..
        :return:
        Espaço percorrido pela melhor solução encontrada.
        """
        # Verificando se alcancei estágio de testar o destino
        if len(path_order) == hops:
            total_distance = path_sum + self.instance.get_destiny_distance(path_order[-1])
            if total_distance < bestw:
                bestw = total_distance
                solution.clear()
                solution = solution.extend(path_order)
        # Ainda não alcancei estágio de verificação do destino
        else:
            for p in passengers:
                path_sum += self.instance.get_distance(path_order[-1], p)
                path_order.append(p)
                passengers_aux = passengers[:]
                passengers_aux.remove(p)
                bestw = self._minimum_path_bt(passengers_aux, path_sum, path_order, solution, bestw, hops)
                path_sum -= self.instance.get_distance(path_order[-1], path_order[-2])
                path_order.pop()
        return bestw

    def solve(self):
        initial_solutions = self._build_initial_solutions()
        final_best = deepcopy(initial_solutions[0])
        results = []
        times = []
        executor = ProcessPoolExecutor()
        for solution in initial_solutions:
            results.append(executor.submit(self._runSimulatedAnnealing, solution))
        for result in results:
            best, time = result.result()
            times.append(time)
            if best.bestw < final_best.bestw:
                final_best = best

        return final_best, sum(times)


    def _runSimulatedAnnealing(self, solution):
        """
        Executa a simulação de recozimento para encontrar uma solução.
        :param solution: Solução inicial.
        :type solution: Solution
        """
        begin = default_timer()
        best_solution = deepcopy(solution)
        temperature = config.initial_temp
        iterT = 0
        maxIter = (self.instance.participants*0.2)
        while temperature > 0.0001:
            while iterT < maxIter:
                iterT += 1
                # Gere um vizinho qualquer
                new_solution = self._update_solution(solution) 
                delta = new_solution.bestw - solution.bestw
                if delta < 0:
                    solution = new_solution
                    if solution.bestw < best_solution.bestw:
                        best_solution = deepcopy(solution)
                        #print(best_solution.bestw)
                else:
                    x = random.uniform(0, 1)
                    if x < math.exp(-delta/temperature):
                        solution = new_solution
            temperature *= config.decrease_temp
            #print('nova temperatura {}'.format(temperature))
            iterT = 0
        return best_solution, default_timer() - begin

    def _update_solution(self, solution):
        """
        Gera uma solução randômica a partir da solução atual.
        :param solution: Solução atual.
        """
        # Seleciona aleatoriamente a operação a ser aplicada na solução
        op = random.randint(0, 4)
        if op == 0:
            return self._relocate_customer(solution)
        elif op == 1:
            return self._exchange_customer(solution)
        elif op == 2:
            return self._exchange_driver(solution)
        elif op == 3:
            return self._allocate_car(solution)
        else:
            return self._deallocate_car(solution)

    def _relocate_customer(self, solution):
        """
        Irá mover um passageiro de um carro para outro.
        :param solution: Solução a ser alterada
        :type solution: Solution
        """
        sol = deepcopy(solution)
        if len(solution.get_groups()) == 1:
            return sol
        drivers = sol.get_drivers()
        # Sorteia um participante que não seja motorista
        plist = [s for s in self.instance.graph.nodes()[1:] if s not in drivers]
        p = random.sample(plist, 1)[0]
        group_orig = sol.get_node_group(p)
        group_dest = group_orig

        maxiter = 10
        # Sorteia um grupo destino
        while maxiter and (group_orig == group_dest or len(group_dest.components) == config.vehicle_capacity):
            group_dest = random.sample(sol.get_groups(), 1)[0]
            maxiter -= 1

        if not maxiter:
            return sol

        # Ajustando grupo de origem
        members_orig = group_orig.components
        members_orig.remove(p)
        sol.remove_group(group_orig)
        sol.add_group(*self._minimum_path(members_orig[0], members_orig[1:]))

        # Ajustando grupo de destino
        members_dest = group_dest.components
        members_dest.append(p)
        sol.remove_group(group_dest)
        sol.add_group(*self._minimum_path(members_dest[0], members_dest[1:]))

        return sol


    def _exchange_customer(self, solution):
        """
        Troca dois passageiros entre si.
        :param solution: Solução a ser modificada
        :type solution: Solution
        """
        sol = deepcopy(solution)
        if len(solution.get_groups()) == 1:
            return sol
        drivers = sol.get_drivers()
        # Sorteia um participante que não seja motorista
        plist = [s for s in self.instance.graph.nodes()[1:] if s not in drivers]
        p1, p2 = random.sample(plist, 2)

        group1 = sol.get_node_group(p1)
        group2 = sol.get_node_group(p2)
        members1 = group1.components
        members2 = group2.components
        

        if group1 == group2:
            return sol

        # Remove dois grupos antigos
        sol.remove_group(group1)
        sol.remove_group(group2)
        members1.remove(p1)
        members1.append(p2)
        members2.remove(p2)
        members2.append(p1)

        sol.add_group(*self._minimum_path(members1[0], members1[1:]))
        sol.add_group(*self._minimum_path(members2[0], members2[1:]))
        return sol

    def _exchange_driver(self, solution):
        sol = deepcopy(solution)
        
        # Sorteia dois motoristas
        plist = [s for s in self.instance.graph.nodes()[1:] if self.instance.is_driver(s)]
        p1, p2 = random.sample(plist, 2)

        group1 = sol.get_node_group(p1)
        group2 = sol.get_node_group(p2)
        members1 = group1.components
        members2 = group2.components
        p1_index = members1.index(p1)
        p2_index = members2.index(p2)

        if group1 == group2:
            if not p1_index or not p2_index:
                sol.remove_group(group1)
                members1[p1_index] = p2
                members1[p2_index] = p1
                sol.add_group(*self._minimum_path(members1[0], members1[1:]))
            return sol
        
        members1[p1_index] = p2
        members2[p2_index] = p1

        # Remove dois grupos antigos
        sol.remove_group(group1)
        sol.remove_group(group2)

        sol.add_group(*self._minimum_path(members1[0], members1[1:]))
        sol.add_group(*self._minimum_path(members2[0], members2[1:]))
        return sol

    def _allocate_car(self, solution):
        # Verifica se podem ser alocados mais carros
        if len(solution.get_groups()) >= self.instance.maxdrivers:
            return solution

        sol = deepcopy(solution)
        drivers_inuse = solution.get_drivers()
        # Sorteia um motorista para novo carro
        plist = [s for s in self.instance.graph.nodes()[1:] if self.instance.is_driver(s) and s not in drivers_inuse]
        new_driver = random.sample(plist, 1)[0]
        new_members = []

        # Atualiza o grupo do motorista escolhido
        group = sol.get_node_group(new_driver)
        members = group.components
        sol.remove_group(group)
        members.remove(new_driver)
        sol.add_group(*self._minimum_path(members[0], members[1:]))
        
        # Escolhe uma quantidade arbitrária de passageiros para o novo veículo
        count = random.randint(1, config.vehicle_capacity-1)
        drivers_inuse = sol.get_drivers()
        plist = [s for s in self.instance.graph.nodes()[1:] if s not in drivers_inuse]
        plist.remove(new_driver)
        while count and plist:
            p = random.sample(plist, 1)[0]
            # Atualiza o grupo do motorista escolhido
            group = sol.get_node_group(p)
            members = group.components
            sol.remove_group(group)
            members.remove(p)
            sol.add_group(*self._minimum_path(members[0], members[1:]))
            new_members.append(p)
            plist.remove(p)
            count -= 1
        sol.add_group(*self._minimum_path(new_driver, new_members))
        #print('adicionou um carro')
        return sol


    def _deallocate_car(self, solution):
        #print(len(solution.get_groups()))
        # Verifica se posso remover um carro
        if len(solution.get_groups()) == self.instance.mindrivers:
            return solution

        sol = deepcopy(solution)
        # Escolhe um grupo para ser removido
        group = random.sample(sol.get_groups(), 1)[0]
        sol.remove_group(group)

        passengers = group.components

        # Sorteia grupos aleatórios para inserir os membros do grupo desfeito   
        while passengers:
            groups = [g for g in sol.get_groups() if len(g.components) < config.vehicle_capacity]
            group = random.sample(groups, 1)[0]
            members = group.components
            sol.remove_group(group)
            members.append(passengers.pop(0))
            sol.add_group(*self._minimum_path(members[0], members[1:]))

        #print('removeu um carro')
        return sol