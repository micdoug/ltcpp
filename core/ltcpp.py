"""
Arquivo: ltcpp.py
Autor: Michael Dougras da Silva

Descrição:
Utiliza as instâncias do artigo original "Tabu search for the real-world carpooling problem".

Changelog:
    31.05.2016  Michael     Criação do arquivo e primeiras adequações para resolução do problema.
    24.06.2016  Michael     Ajuste da classe para utilizar instâncias do artigo original.
"""

# Área de importação
from networkx import Graph
from math import hypot, asin, degrees
import random
from . import config
from math import ceil


class LTCPP:
    """
    Classe base para resolução do problema LTCPP.
    Considera sempre o nó 1 como o destino compartilhado.
    """

    def __init__(self, input, output):
        """
        Construtor da classe de gerenciamento dos dados necessários para resolver o problema LTCPP.
        A quantidade de motoristas disponíveis (D) e quantidade máxima que podem ser usadas é lida do
        arquivo de entrada.
        O problema considera que todos os carros tem capacidade igual, que é configurada no arquivo config.py.
        :param input:
        Nome do arquivo de entrada no formato CVRP.
        :param output:
        Nome do arquivo de saída a ser criado.
        """
        self.input = input
        self.output = output
        
        # Constrói o grafo de distâncias
        self._parse_input()
        nodes = self.graph.nodes()
       
        # Calcula a quantidade mínima de motoristas para resolver o problema
        self.mindrivers = ceil((len(nodes) - 1) / config.vehicle_capacity)

        # Configura o máximo de iterações da busca com base no número de nós
        if (len(nodes) - 1) > 50:
            config.max_iter = 5000

        # Cálcula a soma inicial de todos os participantes até o destino
        self.ini = 0
        for p in nodes[1:]:
            self.ini += self.get_destiny_distance(p)

        self.drivers = [p for p in self.graph.nodes()[1:] if self.is_driver(p)]


    def _parse_input(self):
        """
        A partir do arquivo de entrada, constrói o grafo que modela a instância do problema.
        Esta função considera os arquivos utilizados no artigo original "Tabu search for the 
        real-world carpooling problem."
        """
        with open(self.input, 'r') as file:
            # A primeira linha do arquivo especifica a quantidade total de
            # passageiros (P), a quantidade de motoristas disponíveis (D) e a
            # quantidade máxima de motoristas que podem ser usados (K).
            self.participants, self.ndrivers, self.maxdrivers = [int(s) for s in file.readline().split()]
            
            # Cria um grafo
            self.graph = Graph()

            # A próxima linha do arquivo indica a posição do destino comum, ele
            # é adicionado como vértice 1
            x, y = [int(s) for s in file.readline().split()]
            self.graph.add_node(1, x = x, y = y)
            for i in range(2, self.participants + 2):
                x, y, isdriver = [int(s) for s in file.readline().split()[:-1]]
                self.graph.add_node(i, x = x, y = y, isdriver=bool(isdriver))

        # Calculando distâncias entre os nós
        nodes = self.graph.nodes()
        for i in nodes:
            for j in nodes[nodes.index(i) + 1:]:
                distance = hypot(self.graph.node[i]['x'] - self.graph.node[j]['x'],
                                 self.graph.node[i]['y'] - self.graph.node[j]['y'])
                self.graph.add_edge(i, j, d = distance)

    def get_distance(self, i, j):
        """
        Calcula a distância entre os dois nós.
        :param i: Nó de origem.
        :param j: Nó de destino.
        :return: Distância entre os nós i e j.
        """
        return self.graph.edge[i][j]['d']

    def get_destiny_distance(self, i):
        """
        Calcula a distância entre um nó e o destino em comum.
        :param i: Nó a ser avaliado.
        :return: A distância entre o nó i e o destino em comum.
        """
        return self.graph.edge[i][1]['d']

    def get_path_distance(self, path):
        """
        Calcula o espaço percorrido por um caminho definido pela lista de nós passados.
        :param path: Lista de nós presentes no caminho. Não deve conter o destino.
        :return:
        Espaço percorrido pelo caminho.
        """
        total = 0
        for s in range(len(path) - 1):
            total += self.get_distance(path[s], path[s + 1])
        total += self.get_destiny_distance(path[-1])
        return total

    def is_driver(self, participant):
        """
        Verifica se um participante é um potencial motorista.
        :param participant: Identificador do participante que é testado.
        :return: True se o participante pode ser usado como motorista, False caso contrário.
        """
        return self.graph.node[participant]['isdriver']