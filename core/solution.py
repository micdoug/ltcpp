"""
Arquivo: solution.py
Autor: Michael Dougras da Silva

Descrição:
Definição de estruturas que ajudam a representar a solução atual.

Changelog:
    31.05.2016  Michael     Criação do arquivo e primeiras adequações para resolução do problema.
"""

# Área de importação
from collections import namedtuple

"""
Tipo para armazenar um grupo presente na resposta.
Este tipo armazena basicamente os participantes dentro de um grupo
e a distância gasta por eles.
"""
Group = namedtuple('Group', ['components', 'bestw', 'id'])


class Solution:
    """
    Classe que armazena uma solução para o problema sendo explorada atualmente.
    """

    def __init__(self):
        """
        Inicializa a classe de solução.
        """
        self._groups = []
        self.bestw = 0
        self._group_identifier = 0



    def add_group(self, components, bestw):
        """
        Adiciona um grupo na solução, somando o espaço gasto por ele na resposta.
        :param components:
        Lista de participantes ordenados que definem a rota.
        :param bestw:
        Melhor distância encontrada neste caminho.
        """
        group = Group(components, bestw, self._group_identifier)
        self._group_identifier += 1
        self._groups.append(group)
        self.bestw += bestw

    def get_node_group(self, node):
        """
        Retorna o grupo que um determinado nó está presente.
        :param node:
        Nó a ser pesquisado.
        :return:
        Grupo onde o nó informado está presente.
        """
        for g in self._groups:
            if node in g.components:
                return g

    def remove_group(self, group):
        """
        Remove um grupo da resposta, subtraindo o espaço percorrido do grupo da resposta atual.
        :param group:
        Grupo a ser removido.
        """
        self._groups.remove(group)
        self.bestw -= group.bestw

    def get_drivers(self):
        """
        Recupera os motoristas sendo utilizados nesta solução.
        :return: Lista com os motoristas utilizados nesta solução.
        """
        return [s.components[0] for s in self._groups]

    def get_groups(self):
        """
        Retorna todos os grupos presentes nesta solução.
        :return: Lista com os grupos na solução.
        """
        return self._groups




