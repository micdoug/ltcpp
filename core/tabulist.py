"""
Arquivo: tabulist.py
Autor: Michael Dougras da Silva

Descrição:
Arquivo de implementação da lista tabu.

Changelog:
    01.06.2016  Michael     Criação do arquivo e primeiras adequações para resolução do problema.
"""

from core import config
from collections import deque

class TabuList:
    """
    Implementação da lista tabu a ser utilizada no programa.
    """
    def __init__(self):
        self._list = deque(maxlen=config.tabu_size)

    def can_exchange(self, i, j):
        """
        Verifica se é possível efetuar troca entre i e j
        :param i: Participante do problema
        :param j: Participante do problema
        :return: Se não existe alguma regra na lista tabu que impeça a troca.
        """
        if config.tabu_target == 1:
            return not (i, j) in self._list
        else:
            return i not in self._list and j not in self._list

    def can_relocate(self, i):
        """
        Verifica se é possível mover o participante i.
        :param i: Participante do problema.
        :return: Se não existe alguma regra na lista tabu que impeça o movimento.
        """
        if config.tabu_target == 2:
            return i not in self._list
        else:
            return True

    def register_exchange(self, i, j):
        """
        Registra uma troca entre dois participantes.
        :param i: Partipante do problema.
        :param j: Participante do problema.
        """
        if config.tabu_target == 1:
            self._list.append((i,j))
        else:
            self._list.append(i)
            self._list.append(j)

    def register_relocate(self, i):
        """
        Registra a movimentação do participante i.
        :param i: Participante do problema.
        """
        if config.tabu_target == 2:
            self._list.append(i)
