"""
Arquivo: arguments.py
Autor: Michael Dougras da Silva

Descrição:
Definição de estruturas para tratamento dos parâmetros do programa.

Changelog:
    31.05.2016  Michael     Criação do arquivo e primeiras adequações para resolução do problema.
"""

# Área de importação
import sys

class Arguments:
    """
    Classe responsável por encapsular as informações passadas como parâmetro para o aplicativo.
    Nela são feitas validações quanto a parâmetros obrigatórios. Neste caso somente é verificado
    se foram passados dois parâmetros. Caso não seja validado, lança uma exceção AttributeError.
    """

    def __init__(self):
        """
        Construtor da classe. Faz a validação dos parâmetros passados.
        """
        self._argcount = 2

        # Verifica se a quantidade correta de parâmetros foi passada
        if len(sys.argv)-1 != self._argcount:
            raise AttributeError()

        self._values = tuple(sys.argv[1:])
        self._keys = ('finput', 'foutput')

    def __len__(self):
        return len(self._values)

    def __getitem__(self, item):
        if type(item) == int:
            return self._values[item]
        else:
            index = self._keys.index(item)
            return self._values[index]