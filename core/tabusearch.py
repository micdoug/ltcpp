"""
Arquivo: tabusearch.py
Autor: Michael Dougras da Silva

Descrição:
Implementação dos métodos principais da busca tabu.

Changelog:
    31.05.2016  Michael     Criação do arquivo e primeiras adequações para resolução do problema.
"""

# Área de importação
import math
from core.solution import Solution
from core import config
from core.tabulist import TabuList
import copy
import neighborhood as nb
import random
from concurrent.futures import ProcessPoolExecutor
from timeit import default_timer


class TabuSearch:
    """
    Classe que encontra uma solução aproximada para ltcpp utilizando a busca tabu.
    """

    def __init__(self, instance):
        """
        Construtor da classe.
        :param instance:
         Uma instância da classe LTCPP que contenha um grafo já preparado para ser solucionado.
        :type instance: core.LTCPP
        """
        self._instance = instance
        # armazena as vizinhanças do problema
        self._nbs = []

    def _dp(self, ndrivers):
        """
        Deve ser chamado para construir a solução inicial para um determinado número de motoristas.
        :param ndrivers:
        Número de motoristas a ser testado.
        :return:
        Dicionário com motoristas e passageiros vinculados a ele.
        """
        return self._driver_selection_passenger_assignment(ndrivers, self._instance.graph.nodes()[1:], self._instance.drivers)

    def _driver_selection_passenger_assignment(self, ndrivers, p, d):
        """
        Método de seleção de motoristas e alocação de passageiros.
        :param ndrivers:
        Número de motoristas a ser avaliado.
        :param p:
        Lista de participantes (motoristas + passageiros).
        :param d:
        Lista de motoristas disponíveis.
        :return:
        Lista de motoristas e passageiros atribuídos a eles.
        """
        driver_list = {s: [] for s in random.sample(d, ndrivers)}
        passenger_list = p[:]
        for driver in driver_list:
            passenger_list.remove(driver)

        for passenger in passenger_list:
            min = math.inf
            id = 0
            for driver in driver_list:
                dist = self._instance.graph.edge[passenger][driver]['d']
                if min > dist and (config.vehicle_capacity-1) > len(driver_list[driver]):
                    min = dist
                    id = driver
            driver_list[id].append(passenger)

        return driver_list

    def get_distance(self, i, j):
        """
        Calcula a distância entre os dois nós.
        :param i: Nó de origem.
        :param j: Nó de destino.
        :return: Distância entre os nós i e j.
        """
        return self._instance.graph.edge[i][j]['d']

    def get_destiny_distance(self, i):
        """
        Calcula a distância entre um nó e o destino em comum.
        :param i: Nó a ser avaliado.
        :return: A distância entre o nó i e o destino em comum.
        """
        return self._instance.graph.edge[i][1]['d']

    def _minimum_path(self, driver, passengers):
        """
        Constrói o melhor caminho utilizando como ponto de origem o motorista de um grupo,
        passando por todos os passageiros e chegando no destino.
        :param driver:
        Motorista.
        :param passengers:
        Lista de passageiros.
        :return:
        """
        # Armazenar melhor solução explorada até agora
        solution = []
        bestw = math.inf

        # Armazenar estado atual da busca na árvore de soluções
        path_sum = 0
        path_order = [driver]

        hops = len(passengers) + 1

        bestw = self._minimum_path_bt(passengers, path_sum, path_order, solution, bestw, hops)
        return solution, bestw

    def _minimum_path_bt(self, passengers, path_sum, path_order, solution, bestw, hops):
        """
        Método de tentativa e erro para encontrar o menor caminho a partir de um motorista, passando
        por todos os passageiros e indo até o destino.
        :param passengers:
        Lista de passageiros a serem analisados.
        :param path_sum:
        Soma do caminho atual explorado.
        :param path_order:
        Ordem de visita do caminho explorado atualmente.
        :param solution:
        Armazena a melhor solução atual.
        :param bestw:
        Armazena o espaço gasto pela melhor solução encontrada até agora.
        :param hops:
        Armazena a quantidade máxima de saltos presentes do caminho a ser construído..
        :return:
        Espaço percorrido pela melhor solução encontrada.
        """
        # Verificando se alcancei estágio de testar o destino
        if len(path_order) == hops:
            total_distance = path_sum + self.get_destiny_distance(path_order[-1])
            if total_distance < bestw:
                bestw = total_distance
                solution.clear()
                solution = solution.extend(path_order)
        # Ainda não alcancei estágio de verificação do destino
        else:
            for p in passengers:
                path_sum += self.get_distance(path_order[-1], p)
                path_order.append(p)
                passengers_aux = passengers[:]
                passengers_aux.remove(p)
                bestw = self._minimum_path_bt(passengers_aux, path_sum, path_order, solution, bestw, hops)
                path_sum -= self.get_distance(path_order[-1], path_order[-2])
                path_order.pop()
        return bestw

    def _build_initial_solutions(self):
        """
        Constrói soluções iniciais, onde será executada a busca tabu.
        :return:
        Lista com soluções iniciais.
        """
        solutions = []
        for k in range(self._instance.mindrivers, self._instance.maxdrivers+1):
            s = Solution()
            groups = self._dp(k)
            for g in groups:
                temp = self._minimum_path(g, groups[g])
                s.add_group(*temp)
            solutions.append(s)
        return solutions

    def _do_next_movement(self, solution, tabulist):
        """
        Cria aleatoriamente uma estrutura de vizinhança.
        :param solution: Referẽncia para a solução sendo avaliada.
        :return:
        Vizinhança construída.
        """
        if len(solution.get_groups()) > 1:
            num = random.randint(0, 3)
        else:
            num = random.randint(0, 2)

        # if num == 1:
        #     return nb.CustomerExchangeNH(self._instance, solution, tabulist)
        # elif num == 2:
        #     return nb.CustomerRelocationNH(self._instance, solution, tabulist)
        # elif num == 3:
        #     return nb.DriverExchangeNH(self._instance, solution, tabulist)
        # else:
        #     return nb.RouteExchangeNH(self._instance, solution, tabulist)
        if not len(self._nbs):
            self._nbs.append(nb.CustomerExchangeNH(self._instance, solution, tabulist))
            self._nbs.append(nb.CustomerRelocationNH(self._instance, solution, tabulist))
            self._nbs.append(nb.DriverExchangeNH(self._instance, solution, tabulist))
            if len(solution.get_groups()) > 1:
                self._nbs.append(nb.RouteExchangeNH(self._instance, solution, tabulist))

        nodes = self._nbs[num].do_best_movement()
        # Se foi executado um movimento, recalcula vizinhanças
        if nodes:
            # Assegura que não vai repetir a operação para o mesmo grupo,
            groups = [solution.get_node_group(n) for n in nodes]
            unique = []
            [unique.append(item) for item in groups if item not in unique]
            # Atualiza vizinhança para os grupos afetados
            for group in unique:
                for c in group.components:
                    for nbh in self._nbs[:-1]:
                        nbh.recalculate_movement(c)
            if len(solution.get_groups()) > 1:
                self._nbs[3].recalculate_movement()

    def solve(self):
        ini_solutions = self._build_initial_solutions()
        # Calcula média das soluções iniciais
        self.avg_twostage = sum([s.bestw for s in ini_solutions])/len(ini_solutions)
        self.twostage =[s.bestw for s in ini_solutions]
        # Se não foi possível gerar soluções iniciais, o problema não tem solução
        if not len(ini_solutions):
            return None

        final_best = copy.deepcopy(ini_solutions[0])
        results = []
        times = []
        executor = ProcessPoolExecutor()
        for solution in ini_solutions:
            results.append(executor.submit(self._tabu_search, solution))
        for result in results:
            best, time = result.result()
            times.append(time)
            if best.bestw < final_best.bestw:
                final_best = best

        return final_best, sum(times)

    def _tabu_search(self, solution):
        begin = default_timer()
        # Inicializar lista tabu
        tabulist = TabuList()
        # Armazena a solução inicial
        best = copy.deepcopy(solution)
        # Contadores de iterações
        iter = 0
        unmax_iter = 0
        while iter < config.max_iter and unmax_iter < config.unmax_iter:
            iter += 1
            self._do_next_movement(solution, tabulist)
            if solution.bestw < best.bestw:
                unmax_iter = 0
                best = copy.deepcopy(solution)
            else:
                unmax_iter += 1
        end = default_timer()
        return best, (end-begin)







